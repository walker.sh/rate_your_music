# Scrapy settings for rate_your_music project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'rate_your_music'

SPIDER_MODULES = ['rate_your_music.spiders']
NEWSPIDER_MODULE = 'rate_your_music.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'rate_your_music (+http://www.yourdomain.com)'
